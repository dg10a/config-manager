import filecmp
import logging
import os
import shutil
import sys
from pathlib import Path
from typing import Dict, List, Set, Tuple

import docker

logger = logging.getLogger(__name__)


class ConfigManager:

    LABEL_PREFIX: str = "config-manager"
    MANAGED_CONFIGS_LABEL: str = f"{LABEL_PREFIX}.container.managed-configs"
    CONFIG_NAME_LABEL: str = f"{LABEL_PREFIX}.volume.config-name"

    CONFIG_DATA_PREFIX: Path = Path("/config-data")
    CONFIG_VOLUME_PREFIX: Path = Path("/config-volumes")

    def __init__(self, docker_client) -> None:
        self.docker_client = docker_client

    def run(self) -> None:
        self.refresh_state()
        configs_to_change: Dict[
            str, Tuple[List[Path], List[Path], List[Path]]
        ] = self.preprocess_configs()

        if len(configs_to_change) == 0:
            logger.info("No configs to be updated.")
            return

        containers_to_update: Set[docker.models.containers.Container] = set()
        for volume in self.managed_configs:
            config_name = volume.attrs["Labels"][self.CONFIG_NAME_LABEL]
            if config_name in configs_to_change:
                for container in self.managed_configs[volume]:
                    containers_to_update.add(container)

        stopped_containers: Set[docker.models.containers.Container] = set()
        error_encountered: bool = False

        for container in containers_to_update:
            try:
                container.stop()
                stopped_containers.add(container)
                logger.info(f"Stopped container {container.name}.")
            except docker.errors.APIError as e:
                error_encountered = True
                logger.error(
                    f"Unable to stop container {container.name}, exiting. Error: {e.response.status_code} {e.response.text}"
                )
                break

        if not error_encountered:
            for config in configs_to_change:
                volume_dir: Path = self.get_volume_path(config)
                data_dir: Path = self.get_data_path(config)
                ConfigManager.update_paths(
                    data_dir, volume_dir, configs_to_change[config]
                )

        for container in stopped_containers:
            try:
                container.restart()
                logger.info(f"Restarted container {container.name}.")
            except docker.errors.APIError as e:
                error_encountered = True
                logger.error(
                    f"Unable to restart container {container.name}. Error: {e.response.status_code} {e.response.text}"
                )

        if error_encountered:
            sys.exit("Error(s) encountered.")

    @staticmethod
    def diff_paths(src: Path, dest: Path) -> Tuple[List[Path], List[Path], List[Path]]:
        diff: filecmp.dircmp = filecmp.dircmp(dest, src)
        common_match, common_mismatch, comp_err = filecmp.cmpfiles(
            dest, src, diff.common_files, shallow=False
        )

        to_remove: List[Path] = []
        to_add: List[Path] = []
        stat_only: List[Path] = []

        # stat only
        for path in [Path(x) for x in diff.common]:
            src_path = src.joinpath(path)
            dest_path = dest.joinpath(path)
            if os.stat(src_path).st_mode != os.stat(dest_path).st_mode:
                stat_only.append(path)

        # files in dest that are not in src
        for path in [
            Path(x) for x in (diff.left_only + diff.common_funny + common_mismatch)
        ]:
            to_remove.append(path)

        # subdirs
        for subdir in diff.subdirs:
            subdiff = diff.subdirs[subdir]
            sub_remove, sub_add, sub_stat = ConfigManager.diff_paths(
                Path(subdiff.right), Path(subdiff.left)
            )
            to_remove += [Path(subdir).joinpath(Path(x)) for x in sub_remove]
            to_add += [Path(subdir).joinpath(Path(x)) for x in sub_add]
            stat_only += [Path(subdir).joinpath(Path(x)) for x in sub_stat]

        # files in src that are not in dest
        for path in [
            Path(x) for x in (diff.right_only + diff.common_funny + common_mismatch)
        ]:
            to_add.append(path)

        return to_remove, to_add, stat_only

    def config_ready(self, config_name: str) -> bool:
        ready_to_update: bool = True

        volume_dir: Path = self.get_volume_path(config_name)
        if not volume_dir.exists():
            logger.warning(f"Unable to access volume for config {config_name}.")
            ready_to_update = False

        data_dir: Path = self.get_data_path(config_name)
        if not data_dir.exists():
            logger.warning(f"Unable to access data for config {config_name}.")
            ready_to_update = False

        return ready_to_update

    def get_volume_path(self, config_name: str) -> Path:
        return self.CONFIG_VOLUME_PREFIX.joinpath(f"{config_name}")

    def get_data_path(self, config_name: str) -> Path:
        return self.CONFIG_DATA_PREFIX.joinpath(f"{config_name}")

    def preprocess_configs(
        self,
    ) -> Dict[str, Tuple[List[Path], List[Path], List[Path]]]:
        configs_to_change: Dict[str, Tuple[List[Path], List[Path], List[Path]]] = {}

        for volume in self.managed_configs:
            config_name: str = volume.attrs["Labels"][self.CONFIG_NAME_LABEL]

            if not self.config_ready(config_name):
                logger.warning(f"Unable to process config {config_name}, skipping.")
                continue

            volume_dir: Path = self.get_volume_path(config_name)
            data_dir: Path = self.get_data_path(config_name)

            to_remove, to_add, stat_only = ConfigManager.diff_paths(
                data_dir, volume_dir
            )
            if len(to_remove) == 0 and len(to_add) == 0 and len(stat_only) == 0:
                continue
            else:
                configs_to_change[config_name] = (to_remove, to_add, stat_only)

        return configs_to_change

    @staticmethod
    def update_paths(
        src: Path, dest: Path, diff: Tuple[List[Path], List[Path], List[Path]]
    ) -> None:
        to_remove, to_add, stat_only = diff

        for path in to_remove:
            dest_path = dest.joinpath(path)
            if dest_path.is_dir():
                shutil.rmtree(dest_path)
            else:
                dest_path.unlink()
            logger.info(f"Removed path {path}")

        for path in to_add:
            src_path = src.joinpath(path)
            dest_path = dest.joinpath(path)
            if src_path.is_dir():
                shutil.copytree(src_path, dest_path)
            else:
                shutil.copyfile(src_path, dest_path)
            logger.info(f"Added path {path}")

        for path in to_add + stat_only:
            src_path = src.joinpath(path)
            dest_path = dest.joinpath(path)
            shutil.copystat(src_path, dest_path)
            logger.info(f"Updated stat for {path}")

    def refresh_state(self) -> None:
        """Refresh managed configs (volumes that are attached to a container and have the managed config label)"""
        self.managed_configs: Dict[
            docker.models.volumes.Volume, Set[docker.models.containers.Container]
        ] = {}

        for volume in self.docker_client.volumes.list():
            if (
                volume.attrs["Labels"]
                and self.CONFIG_NAME_LABEL in volume.attrs["Labels"]
            ):
                logger.info(f"Found managed volume {volume.name}.")
                if volume not in self.managed_configs:
                    self.managed_configs[volume] = set()

        for container in self.docker_client.containers.list():
            if self.MANAGED_CONFIGS_LABEL in container.labels:
                logger.info(f"Found container {container.name} with managed configs.")
                volumes = container.labels[self.MANAGED_CONFIGS_LABEL].split(",")
                for mount in container.attrs["Mounts"]:
                    if mount["Type"] == "volume":
                        volume = self.docker_client.volumes.get(mount["Name"])
                        if (
                            volume.attrs["Labels"]
                            and self.CONFIG_NAME_LABEL in volume.attrs["Labels"]
                            and volume.attrs["Labels"][self.CONFIG_NAME_LABEL]
                            in volumes
                        ):
                            logger.info(
                                f"Managed volume {volume.name} is attached to container {container.name}."
                            )
                            self.managed_configs[volume].add(container)

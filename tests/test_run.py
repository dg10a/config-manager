import os
import subprocess
import tarfile
import tempfile
import time
from pathlib import Path

import docker
import pytest
from config_manager import ConfigManager

COMPOSE_PROJECT_LABEL = "com.docker.compose.project"


def current_dir():
    path_to_current_file = os.path.realpath(__file__)
    _current_dir = os.path.split(path_to_current_file)[0]
    return _current_dir


@pytest.fixture(scope="session")
def docker_client():
    return docker.from_env()


@pytest.fixture(scope="session")
def configs_image(docker_client):
    docker_client.images.build(
        path=os.path.join(current_dir(), ".."), tag="config-manager"
    )
    return


@pytest.fixture(scope="session")
def containers(
    configs_image, docker_client, docker_services, docker_compose_project_name
):
    for container in docker_client.containers.list():
        if COMPOSE_PROJECT_LABEL in container.labels:
            if container.labels[COMPOSE_PROJECT_LABEL] == docker_compose_project_name:
                if ConfigManager.MANAGED_CONFIGS_LABEL in container.labels:
                    test_container = container
                else:
                    configs_container = container

    return test_container, configs_container


@pytest.fixture(scope="session")
def configs_container_exited(containers):
    test_container, configs_container = containers

    start_time = time.time()

    while time.time() - start_time < 120:
        configs_container.reload()
        if configs_container.status == "exited":
            break

        time.sleep(1)

    return


@pytest.fixture(scope="session")
def container_config_path(containers, configs_container_exited):
    test_container, configs_container = containers

    with tempfile.TemporaryFile(suffix=".tar") as temp_archive:
        test_container.reload()
        bits, stat = test_container.get_archive("/config")
        for chunk in bits:
            temp_archive.write(chunk)

        temp_archive.flush()
        temp_archive.seek(0)

        with tempfile.TemporaryDirectory() as temp_dir:
            with tarfile.open(fileobj=temp_archive, mode="r") as tar:
                tar.extractall(path=temp_dir)

            container_config_path = os.path.join(temp_dir, "config")
            yield container_config_path


@pytest.fixture(scope="session")
def host_config_path():
    return os.path.join(current_dir(), "configs/test-config")


def test_container_state(containers, configs_container_exited):
    test_container, configs_container = containers

    test_container.reload()
    assert test_container.status == "running"


def test_diff_subprocess(host_config_path, container_config_path):
    output = subprocess.check_output(
        ["diff", "-r", host_config_path, container_config_path]
    )

    assert output == b""


def test_diff_function(host_config_path, container_config_path):
    to_remove, to_add, stat_only = ConfigManager.diff_paths(
        Path(host_config_path), Path(container_config_path)
    )

    assert len(to_remove) == 0
    assert len(to_add) == 0
    assert len(stat_only) == 0

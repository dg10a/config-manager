# config-manager

A tool for managing container configuration volumes.

## Usage

Create a `configs` directory containing the following Dockerfile:

```dockerfile
FROM config-manager:latest
```

For each unique configuration named `$CONFIG_NAME`:

1. Create a named volume with the label `config-manager.volume.config-name=$CONFIG_NAME`
2. In the `configs` directory create a `$CONFIG_NAME` subdirectory containing the configuration data.

Add the label `config-manager.container.managed-configs` to each container that should be managed by `config-manager`. The value is a comma-separated list of configuration names which should trigger a restart of the container when new configuration data is pushed.

See the [`tests`](./tests) directory for an example.

FROM python:3-alpine

RUN apk update \
	&& apk add --no-cache \
	musl-dev \
	gcc \
	libffi-dev \
	openssl-dev

RUN pip3 install poetry

WORKDIR /app
COPY pyproject.toml poetry.lock /app/
RUN poetry install --no-dev # Install dependencies
COPY . /app
RUN poetry install --no-dev # Install root

ONBUILD COPY . /config-data
CMD ["poetry", "run", "bin/config-manager"]
